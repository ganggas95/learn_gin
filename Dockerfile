FROM golang:onbuild

ARG app_env
ENV APP_ENV $app_env

#COPY app /go/src/gitlab.com/ganggas95/learn_gin
WORKDIR /go/src/gitlab.com/ganggas95/learn_gin

RUN go get ./
RUN go build

EXPOSE 8080
